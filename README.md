# Algoritmos y Programación I (95.11/75.02)

Este repositorio contiene los archivos de clases de laboratorios, de la
materia "Algoritmos y Programación I (95.11/75.02)", perteneciente a la
carrera Ingeniería Electrónica, dictada en la
[Facultad de Ingeniería][fiuba] de la [UBA][uba], correspondiente a la
cátedra de la Lic. Kuhn.

## Contribución

Si sos estudiante, tenés que crear una rama que sea tu número de padrón,
como nombre de la rama, y subir ahí lo que hagas en los ejercicios del
laboratorio.

Si sos docente, subí los archivos a ser descargados en las clases de
laboratorio, de manera ordenada, por ejemplo separando las guías en
distintos directorios y cada archivo con un nombre que lo identifique..
¡nada de clase_01.c, ó ejemplo04.c, ó jueves_30.c, etc.!

[fiuba]: http://www.fi.uba.ar/
[uba]: http://www.uba.ar/